#!/bin/bash
#
# This is a very barebones build script for the C++ dependencies.

# Define green color (see http://stackoverflow.com/questions/5947742/how-to-change-the-echo-output-color-in-linux)
export green='\e[0;32m'
export NC='\e[0m' # No Color]]'
# Build the PDDL generator
echo -e "${green}Building PDDL generator...${NC}"
g++ pddlgenerator/generatePDDL.cpp -o bin/generatePDDL
echo -e "${green}PDDL generator successfully built!${NC}"

# Build the planner
cd spacebookplanner/toolkit/lib/libff
echo -e "${green}Building libff library...${NC}"
make clean
make depend
make libff
echo -e "${green}libff successfully built!${NC}"
echo -e "${green}Building AIG planner...${NC}"
cd ../..
scons --clean
scons
echo -e "${green}AIG planner successfully built!${NC}"
echo -e "${green}Building spacebook planner...${NC}"
cd ../planner
scons --clean
scons
cd ../..
mv spacebookplanner/planner/spacebookPlanner bin/
echo -e "${green}spacebook planner successfullt built!${NC}"
