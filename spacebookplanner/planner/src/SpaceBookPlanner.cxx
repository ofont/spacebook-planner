#include "SpaceBookPlanner.hxx"

SpaceBookPlanner::SpaceBookPlanner(char *domainFile, char *problemFile){
	aig_tk::FF_PDDL_To_STRIPS adl_compiler;
	std::string domain(domainFile);
	std::string instance(problemFile);
        /**
	 * PARSING
	 * Parser that creates the problem representation given a domain and an instance pddl file
	*/
	adl_compiler.get_problem_description(domain, instance, strips_prob, true);
	estimator.initialize(strips_prob);
}



void SpaceBookPlanner::parsePlanOptions(std::string in){
  std::vector<std::string> opts_s = parseString(in);
  unsigned i = 0;
  opts = PlanOptions();
  opts.intermediate_nodes.clear();
  while(i < opts_s.size()){
    if(opts_s[i] == "-i"){
        opts.initial_node  = to_num(opts_s[i+1]);
	i++;
    }
    else if(opts_s[i] == "-g"){
        opts.goal_node  = to_num(opts_s[i+1]);
	i++;
    }
    else if(opts_s[i] == "-v"){
      opts.intermediate_nodes = parseIntermediateNodes(opts_s[i+1]);
      i++;
    }
    i++;    
  }
}


std::string SpaceBookPlanner::getPlanWithIntermediateNodes(int * totalCost){
	std::vector<int> 	intermediate_nodes = opts.intermediate_nodes;
	std::stringstream 	out;
	
	//init the initial state with initial node =)	
	aig_tk::Fluent_Vec 	current_init_goal;	
	std::string 		init_predicate = std::string("AT-PEDESTRIAN-")  + to_string(opts.initial_node);
	int 			predicate_idx = strips_prob.getFluentIndex(init_predicate);	
	current_init_goal.push_back(predicate_idx);
	
	
	//greedy search of the plan
	aig_tk::Regression_Best_First_Search 	engine;
	std::string 				intermediate_goal_predicate_s;
	int 					intermediate_goal_idx;
	aig_tk::Fluent_Vec 			intermediate_goal,intermediate_goal_selected; 
	aig_tk::Cost_Type 			crr_cost,plan_total_cost = 0;
	aig_tk::Cost_Type 			min_cost;
	int 					selected_intermediate_node;
	while(intermediate_nodes.size() != 0){
		aig_tk::STRIPS_Problem::set_init( strips_prob, current_init_goal );		
		estimator.initialize(strips_prob);
		estimator.compute_without_supporters( current_init_goal );
		min_cost 			= 9999999999;
		std::cout << "Init:  " <<  current_init_goal[0] << std::endl;
		for(unsigned i = 0; i < intermediate_nodes.size(); ++i){
			intermediate_goal.clear();		
			intermediate_goal_predicate_s 	= std::string("AT-PEDESTRIAN-")  + to_string(intermediate_nodes[i]);	
			intermediate_goal_idx 		= strips_prob.getFluentIndex(intermediate_goal_predicate_s);	
			intermediate_goal.push_back(intermediate_goal_idx);	
			crr_cost 			= estimator.eval( intermediate_goal );
			std::cout <<"Cost "<< crr_cost  <<" "<< intermediate_goal.size() <<" "<<  intermediate_goal[0] << " "<<
intermediate_goal_predicate_s <<std::endl;
			if(min_cost > crr_cost && crr_cost>0){
				min_cost = crr_cost;
				selected_intermediate_node = i;
				intermediate_goal_selected = intermediate_goal;
			}
		}
		//SEARCH THE SUBPLAN		
		aig_tk::STRIPS_Problem::set_goal( strips_prob, intermediate_goal_selected, false );
		engine.set_heuristic(estimator);
		engine.set_problem(strips_prob);
		aig_tk::Node			*n0 = aig_tk::Node::root( strips_prob, true );	
		std::vector< aig_tk::Node* > 	plan;
		if( engine.solve(n0, plan) )
	  	{
            		std::cout << "Solved subplan"  << std::endl;
	    		std::reverse( plan.begin(), plan.end() );   
	  	}
		else
	 		std::cerr<<"FAILED"<<std::endl;
		//PLAN STRING
		for ( unsigned k = 0; k < plan.size(); k++ )
	 	{
			aig_tk::Action* a = plan[k]->op(); 
			out <<  a->signature() << std::endl;
	 	}        
//		out << std::endl;

		//sum cost to intermediate node to the total plan cost
		plan_total_cost += min_cost;			
		//take the selected node as new initial node to compute the heuristic
		current_init_goal.clear();
		current_init_goal = intermediate_goal_selected;
	       	
		//erase the selcted node from intermediate_nodes
		intermediate_nodes.erase(intermediate_nodes.begin() + selected_intermediate_node);
	}
	
	//extract plan from the last intermediate node to absolute goal
	aig_tk::STRIPS_Problem::set_init( strips_prob, current_init_goal );
	int cost;
	out << getPlanWithoutIntermediateNodes(&cost);
	*totalCost = plan_total_cost + cost;
	std::cout << "Costnf: " << (*totalCost)<< std::endl;
	return out.str();

}

std::string SpaceBookPlanner::getPlanWithoutIntermediateNodes(int * totalCost){
	
	aig_tk::Fluent_Vec goal;
	std::string goalPredicate= std::string("AT-PEDESTRIAN-")+ to_string(opts.goal_node);
	int   predicateGIdx = strips_prob.getFluentIndex(goalPredicate);
	goal.push_back(predicateGIdx);

	
	aig_tk::STRIPS_Problem::set_goal( strips_prob, goal, false );

	estimator.compute_without_supporters( strips_prob.init() );
	aig_tk::Cost_Type goal_cost = estimator.eval( strips_prob.goal()  );
	/**	
       	 * SEARCH
       	 **/
	aig_tk::Regression_Best_First_Search engine;
	engine.set_heuristic(estimator);
	engine.set_problem(strips_prob);
	aig_tk::Node* n0 = aig_tk::Node::root( strips_prob, true );

	std::vector< aig_tk::Node* > plan;
	if( engine.solve(n0, plan) )
	  {
            std::cout << "Solved" << std::endl;
	    std::reverse( plan.begin(), plan.end() );   
	  }
	else
	  std::cerr<<"FAILED"<<std::endl;
	//PLAN STRING
	std::stringstream out;
	for ( unsigned k = 0; k < plan.size(); k++ )
	  {
		aig_tk::Action* a = plan[k]->op(); 
		out <<  a->signature() << std::endl;
	  }        
	std::cout << "Costn: " << goal_cost;
	*totalCost = goal_cost;
	return out.str();
}


std::string SpaceBookPlanner::getPlan(){
	aig_tk::Fluent_Vec init;
	std::string initPredicate = std::string("AT-PEDESTRIAN-")  + to_string(opts.initial_node);
		
	int   predicateIIdx = strips_prob.getFluentIndex(initPredicate);
	init.push_back(predicateIIdx);
	aig_tk::STRIPS_Problem::set_init( strips_prob, init );

	int totalCost;	
	std::string plan;
	
	//Initializing init and goal fluents
	if(opts.intermediate_nodes.size()==0)
		plan = getPlanWithoutIntermediateNodes(&totalCost);
	else
		plan = getPlanWithIntermediateNodes(&totalCost);
	plan+= ("Cost\n" + to_string(totalCost) + "\n");
	
	return plan; 
	
}
	 
   
	

