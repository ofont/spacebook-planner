#ifndef _SPACEBOOKPLANNER_H
#define _SPACEBOOKPLANNER_H

#include <planning/FF_PDDL_To_STRIPS.hxx>
#include <planning/STRIPS_Problem.hxx>
#include <planning/heuristics/Max_Heuristic.hxx>
#include <search/Node.hxx>
#include <search/Regression_Best_First_Search.hxx>

#include <stdio.h>
#include <sstream>

typedef struct planOptions{
  int initial_node;
  int goal_node;
  std::vector<int> intermediate_nodes;
  //other options
  //....
  //....
}PlanOptions;
  


class SpaceBookPlanner{
 protected:
  aig_tk::STRIPS_Problem	  strips_prob;
  aig_tk::Max_Heuristic           estimator;
  std::string getPlanWithIntermediateNodes(int * totalCost);
  std::string getPlanWithoutIntermediateNodes(int * totalCost);
 public:
  SpaceBookPlanner(char *domainFile,char* problemFile);
  std::string getPlan();
  void parsePlanOptions(std::string in);
  PlanOptions opts;

};


//inline parse functions
inline int to_num(std::string s){
 std::stringstream ss(s);
  int i;
  ss >> i;
  return i;
}

inline std::string to_string (int num)
{
  std::stringstream ss;
  ss << num;
  return ss.str();
}


inline std::vector<std::string> parseString(std::string s ){
  std::vector<std::string> opts;
  std::stringstream iss(s);
  do
  {
    std::string sub;
    iss >> sub;
    opts.push_back(sub);
   }while (iss);
   
   return opts;
}


inline std::vector<int> parseIntermediateNodes(std::string is){
  std::vector<int> intermediate_nodes;
  std::stringstream ss(is);
  std::string item;
  
  while(std::getline(ss, item, '-')) {
    intermediate_nodes.push_back(to_num(item));
    }
    return intermediate_nodes;
  
}

#endif
