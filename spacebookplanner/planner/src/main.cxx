// spacebook planner
// ==================
//
// Abstract:
// Calculates best route between 2 or more nodes.
//
// Usage:
// Takes PDDL domain and problem as parameters (created with generatorPDDL).
// Reads plan petitions from stdin and returns a plan. The request format is:
//     -i [input node] -g [goal node] -v [midway nodes]
// -v is an optional parameter.
//
// Result:
// Returns a list of moves from the input node to the goal and the cost written
// to stdout. When the planner is loaded, prints ' --- OK.\n' to stdout too.
// For example, this would be the output for '-i 155568 -g 155577'
//    (MOVE-155568-7172897 )
//    (MOVE-7172897-1478208880 )
//    (MOVE-1478208880-155577 )
//    Cost
//    322
// Just be aware that if the nodes don't exist, it is very possible that the
// planner will return some (apparently) valid plan. So it is always necessary
// to check that the input, goal, and midway nodes are reached before delivering
// the answer as valid.
// If the nodes don't exist, or are repeated, or some strange combinations of
// those facts, it is also possible that the planner segfaults.
//

#include <iostream>
#include "SpaceBookPlanner.hxx"


int main(int argc, char *argv[]) {
    if (argc != 3) {
        std::cout << "Invalid arguments. Call is as follows:" << std::endl;
        std::cout << "./spacebookPlanner domain problem" << std::endl;
        std::cout << "\tdomain: input file. PDDL file containing problem domain." << std::endl;
        std::cout << "\tproblem: input file. PDDL file containing problem instance." << std::endl;
        return 1;
    }
    SpaceBookPlanner planner = SpaceBookPlanner(argv[1], argv[2]);
    std::cout << std::endl;
    // This is a little bit of a hack. Make couts fail, so the legacy code can't
    // populate stdin with undesired results.
    // See: http://stackoverflow.com/q/15800705
    std::cout.setstate(std::ios::failbit);
    std::string input;
    while(std::getline(std::cin, input)) {
        planner.parsePlanOptions(input);
        std::string plan = planner.getPlan();
        std::cout.clear(); // Allow writes to stdin
        std::cout << plan;
        std::cout.setstate(std::ios::failbit); // Disallow them, once again
    }
    return 0;
}
