#!/usr/bin/env python

import sys, traceback, Ice

Ice.loadSlice('Planner.ice')
import Spacebook

try:
    props = Ice.createProperties([])
    props.setProperty("Ice.MessageSizeMax", "65535")
    props.setProperty("Ice.Trace.Network", "1")
    config = Ice.InitializationData()
    config.properties = props
    communicator = Ice.initialize(sys.argv, config)
    planner = Spacebook.PlannerPrx.checkedCast(communicator.stringToProxy("planner:tcp -h localhost -p 10000"))
    # Start testing interface
    # Only load model if it has not been already done
    if "STOCKHOLM" not in planner.getCities():
        model = open("STOCKHOLM_model.pl", "r").read()
        planner.loadCity("stockholm", model)
    # Print loaded cities
    print planner.getCities()
    # Get plan without intermediate nodes
    print planner.getPlan("Stockholm", 121940, 121945, None)
    # Get plan with intermediate nodes
    print planner.getPlan("Stockholm", 121940, 121945, [121942, 121944])
    # Get plan with false nodes
    try:
        print planner.getPlan("Stockholm", 121940, 121945, [1,2,3])
    except Spacebook.NodeError:
        print "Received NodeError as expected"
except:
    traceback.print_exc()
    sys.exit(1)
