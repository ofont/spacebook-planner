#!/usr/bin/env python

import sys
import os
import traceback
import Ice
import subprocess
import re

Ice.loadSlice('Planner.ice')

import Spacebook

class PlannerProcess(object):
    """This object takes a city name and a model and build a PDDL domain and a
    problem from that. Once the domain and PDDL have been built, it launches a
    planner, that will keep listening for plans.
    """
    def __init__(self, city, model):
        """Load the city, create the domain and the problem, start the
        planner.
        """
        # Establish the city
        self.city = city.upper()
        # Establish directories where binaries and data is stored
        self.basedir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        self.bindir = os.path.join(self.basedir, 'bin')
        self.datadir = os.path.join(self.basedir, 'cities/' + self.city)
        # Compute model, domain and problem paths
        self.modelpath = os.path.join(self.datadir, self.city + '_model.pl')
        self.domainpath = os.path.join(self.datadir, self.city + '_domain.pddl')
        self.problempath = os.path.join(self.datadir, self.city +
                                       '_problem.pddl')
        # Create the data directory if it doesn't exist
        if not os.path.exists(self.datadir):
            os.makedirs(self.datadir)
        # Archive model
        with open(self.modelpath, 'w') as f:
            f.write(model)
        self.generate_and_store_pddl()
        # Once archived, start planner
        self.planner = self.startPlanner()
        # And this should be it

    def generate_and_store_pddl(self):
        generator = os.path.join(self.bindir, 'generatePDDL')
        print generator
        result = subprocess.call([
            generator,
            self.modelpath,
            self.domainpath,
            self.problempath
        ])
        if result:  # Anything != 0 is an error
            raise Exception("Something has gone wrong in the PDDL generation.")

    def startPlanner(self):
        plannerbin = os.path.join(self.bindir, 'spacebookPlanner')
        planner = subprocess.Popen(
            [plannerbin, self.domainpath, self.problempath],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE
        )
        # Read first line and ensure it has loaded fine:
        if (
            planner.poll() or
            not planner.stdout.readline() == ' --- OK.\n'
        ):
            raise Exception("Planner hasn't loaded or is dead.")
        else:
            print "Planner loaded!"
            return planner

   
    def getPlan(self, origin, destination, midway=None):
        def parseMove(line):
            """Parses a line with a planer action and returns a tuple with the
            start and end node for the move.
            """
            # This regex detects the start and end of a move
            regex = re.compile(r'\(MOVE\-(.*)\-(.*) \)')
            match = regex.search(line)
            results = tuple(int(n) for n in match.groups()) if match else None
            return results

        def parseSteps(stdout):
            """Parses the planner stdout and recovers the steps returned by the
            path. They are parsed into a list of ints.
            This function also checks that origin, destination and midway nodes
            appear correctly in the steps result.
            """
            # This regex detects the start and end of a move
            steps = []
            line = stdout.readline()
            move = parseMove(line)
            # Add both nodes since it is the first update
            steps.extend(move)
            # Parse second move
            move = parseMove(stdout.readline())
            while move:  # Parse until no more moves available
                steps.append(move[1])
                move = parseMove(stdout.readline())
            # Check that start and end nodes are correct
            startEndOK = (steps[0] == origin and steps[-1] == destination)
            # Check that midway nodes are in
            midwayOK = True
            for node in midway:
                if node not in steps:
                    midwayOK = False
                    break
            if not (startEndOK and midwayOK):
                # Consume the rest of stdout before raising the exception
                stdout.readline()
                raise Spacebook.NodeError
            return steps
        # Start of getPlan method.
        # Sanitize input
        origin = int(origin)
        destination = int(destination)
        command = "-i {0} -g {1}".format(origin, destination)
        if midway:
            midway = [int(node) for node in midway]
            midwaystr = " ".join(str(node) for node in midway)
            command += " -v {0}".format(midwaystr)
        # Write command to planner process
        self.planner.stdin.write(command + '\n')
        # Check that the process has not died
        if self.planner.poll() is not None:
            print self.planner.poll()
            self.planner = self.startPlanner()
            raise Spacebook.NodeError
        # Parse output
        plan = Spacebook.Plan()
        plan.steps = parseSteps(self.planner.stdout)
        plan.cost = int(self.planner.stdout.readline())
        return plan

    def shutdown(self):
        """Shutdown any lingering subprocess before discarding the object."""
        self.planner.kill()

    def __del__(self):
        self.shutdown()


class PlannerInterface(Spacebook.Planner):
    def __init__(self):
        self.planners = {}

    def loadCity(self, city, model, current=None):
        city = city.upper()
        if city in self.planners:
            self.planners[city].shutdown()
        self.planners[city] = PlannerProcess(city, model)

    def getCities(self, current=None):
        return self.planners.keys()

    def getPlan(self, city, origin, destination, midway, current=None):
        city = city.upper()
        if city not in self.planners:
            raise Spacebook.CityError
        return self.planners[city].getPlan(origin, destination, midway)

if __name__ == '__main__':
    # Configure ICE and allow messages up to 65MB (for big models)
    props = Ice.createProperties([])
    props.setProperty("Ice.MessageSizeMax", "65535")
    props.setProperty("Ice.Trace.Network", "2")
    id = Ice.InitializationData()
    id.properties = props
    try:
        # Start a communicator with custom configuration
        communicator = Ice.initialize(id)
        adapter = communicator.createObjectAdapterWithEndpoints("Planner", "tcp -p 10000")
        adapter.add(PlannerInterface(), communicator.stringToIdentity("planner"))
        adapter.activate()
        communicator.waitForShutdown()
        communicator.destroy()
    except:
        traceback.print_exc()
        sys.exit(1)

