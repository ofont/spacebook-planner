// **********************************************************************
//
// Spacebook ICE interface for regression planner
//
// **********************************************************************

#pragma once

module Spacebook
{

// Raised when asking for an unknown city
exception CityError {
    string reason = "Unknown city";
};

// Raised when asking for an unknown node
exception NodeError {
    string reason = "Unknown node";
};

sequence<string> StringSeq;
sequence<int> IntSeq;

// A plan consists of a cost and a sequence of nodes, from origin to
// destination, passing between midway nodes if specified
struct Plan {
    int cost;
    IntSeq steps;
};

// The planner interface. Allows 3 actions:
//     - load cities from a .pl model
//     - get the names of the currently loaded cities
//     - get a plan for a currently loaded city
interface Planner {
    // Load a model (.pl file content) into a PDDL, so the planner has a domain
    // to trace plans on.
    // city will serve as an id for the model. city is transformed to uppercase
    // in the server, so Stockholm, stockholm, StockHOlm (etc.) all refer to
    // STOCKHOLM.
    // Further calls to loadCity with the same city name will overwrite the
    // previous city instance.
    void loadCity(string city, string model);

    // Return a sequence with the names of the currently loaded cities
    StringSeq getCities();

    // Return a plan that connects the nodes origin and destination. It may also
    // take a sequence of nodes to act as waypoints in the trajectory.
    // If no midway points are needed, pass an empty sequence or a null
    // reference in place of the parameter.
    // This call may raise exceptions if it is unable to locate the city or the
    // requested nodes.
    Plan getPlan(string city, int origin, int destination, IntSeq midway)
        throws CityError, NodeError;
};

};
