(define (problem spaceBook-ex)
	(:domain spaceBook)
	(:init
		(at-pedestrian-121937)
		(= (total-cost) 0)
	)
	(:goal
		(and (at-pedestrian-1811206050))
	)
	(:metric minimize (total-cost))
)
