
#include "Node.hxx"

void writePDDL( CityModel &cm, std::string domain, std::string problem) {
	ofstream f( domain.c_str() );
	f << "(define (domain spaceBook)\n";
	f << "\t(:requirements :strips :action-costs)\n";
	f << "\t(:predicates\n";
	for ( ObjectMap::iterator i = cm.objects.begin(); i != cm.objects.end(); ++i )
		if ( i->second->object == "node" )
			f << "\t\t(at-pedestrian-" << i->second->id << ")\n";
	f << "\t)\n";
	f << "\t(:functions\n";
	f << "\t\t(total-cost)\n";
	f << "\t)\n";
	for ( ObjectMap::iterator i = cm.objects.begin(); i != cm.objects.end(); ++i )
		if ( i->second->object == "edge" ) {
			Edge *edge = static_cast< Edge* >( i->second );
			map< unsigned, vector< unsigned > >::iterator j;
			for ( j = edge->segments.begin(); j != edge->segments.end(); ++j ) {
				Node *m = cm.getNode( j->first );
				for ( unsigned k = 0; k < j->second.size(); ++k ) {
					double dist = m->dist( cm.getNode( j->second[ k ] ) );
					f << "\t(:action move-" << j->first << "-" << j->second[ k ] << "\n";
					f << "\t\t:precondition (and (at-pedestrian-" << j->first << "))\n";
					f << "\t\t:effect       (and (at-pedestrian-" << j->second[ k ] << ")\n";
					f << "\t\t                   (not (at-pedestrian-" << j->first << "))\n";
					f << "\t\t                   (increase (total-cost) " << dist << "))\n";
					f << "\t)\n";
				}
			}
		}
	f << ")\n";
	f.close();

	f.open( problem.c_str() );
	f << "(define (problem spaceBook-ex)\n";
	f << "\t(:domain spaceBook)\n";
	f << "\t(:init\n";
	f << "\t\t(at-pedestrian-" << cm.firstNode()->id << ")\n";
	f << "\t\t(= (total-cost) 0)\n";
	f << "\t)\n";
	f << "\t(:goal\n";
	f << "\t\t(and (at-pedestrian-" << cm.lastNode()->id << "))\n";
	f << "\t)\n";
	f << "\t(:metric minimize (total-cost))\n";
	f << ")\n";
	f.close();
}

int main(int argc, char const *argv[]) {
	if (argc != 4) {
		std::cout << "Invalid arguments. Call is as follows:" << std::endl;
		std::cout << "./generatePDDL model domain problem" << std::endl;
		std::cout << "\tmodel: input file. Prolog file containing map data." << std::endl;
		std::cout << "\tdomain: output file. PDDL file containing problem domain." << std::endl;
		std::cout << "\tproblem: output file. PDDL file containing problem instance." << std::endl;
		return 1;
	}
	std::string model, domain, problem;
	model = argv[1];
	domain = argv[2];
	problem = argv[3];
	CityModel cm;
	cm.parse( model );
	writePDDL( cm, domain, problem );
	return 0;
}
