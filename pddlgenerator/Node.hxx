
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

#define MIN( a, b ) ( ( a ) < ( b ) ? ( a ) : ( b ) )

using namespace std;

class Object {
public:
	unsigned id;
	string type;
	string object;

	Object( unsigned i, string o ) : id( i ), object( o ) {}
};

class Edge : public Object {
public:
	map< unsigned, vector< unsigned > > segments;

	Edge( unsigned e, string o ) : Object( e, o ) {}
	void insertSegment( unsigned from, unsigned to ) {
		map< unsigned, vector< unsigned > >::iterator i = segments.find( from );
		if ( i == segments.end() )
			i = segments.insert( segments.begin(), make_pair( from, vector< unsigned >() ) );
		i->second.push_back( to );
	}
};

class Node : public Object {
public:
	double x, y;
	bool junction;
	vector< Edge* > edges;

	Node( unsigned n, string o ) : Object( n, o ), junction( false ) {}
	double dist( Node *n ) {
		return sqrt( ( x - n->x ) * ( x - n->x ) + ( y - n->y ) * ( y - n->y ) );
	}
};

typedef map< unsigned, Object* > ObjectMap;

class CityModel {
public:
	ObjectMap objects;

	~CityModel() {
		for ( ObjectMap::iterator i = objects.begin(); i != objects.end(); ++i )
			delete i->second;
	}

	static int delim( int k, string line ) {
		unsigned x = line.find( ',', k ), y = line.find( ')', k );
		return MIN( x, y );
	}

	static vector< string > parseParams( int k, string line ) {
		char c = ',';
		vector< string > result;
		for ( int e = delim( k, line ); c != ')'; c = line[ e ], e = delim( k = e+1, line ) )
			result.push_back( line.substr( k, e-k ) );
		return result;
	}

	void setType( unsigned o, string t ) {
		ObjectMap::iterator i = objects.find( o );
		if ( i != objects.end() ) i->second->type = t;
	}

	Edge * getEdge( unsigned e ) {
		ObjectMap::iterator i = objects.find( e );
		if ( i == objects.end() )
			i = objects.insert( objects.begin(), make_pair( e, new Edge( e, "edge" ) ) );
		return static_cast< Edge* >( i->second );
	}

	Node * getNode( unsigned n ) {
		ObjectMap::iterator i = objects.find( n );
		if ( i == objects.end() )
			i = objects.insert( objects.begin(), make_pair( n, new Node( n, "node" ) ) );
		return static_cast< Node* >( i->second );
	}

	Node * firstNode() {
		for ( ObjectMap::iterator i = objects.begin(); i != objects.end(); ++i )
			if ( i->second->type == "node" ) return static_cast< Node* >( i->second );
		return 0;
	}

	Node * lastNode() {
		for ( ObjectMap::reverse_iterator i = objects.rbegin(); i != objects.rend(); ++i )
			if ( i->second->type == "node" ) return static_cast< Node* >( i->second );
		return 0;
	}

	void parse( string filename ) {
		string line;
		unsigned e = 0;
		set< string > types;
		ifstream file( filename.c_str() );
		getline( file, line );
		while ( getline( file, line ) ) {
			int x = line.find( '(' );
			string pred = line.substr( 0, x );
			vector< string > p = parseParams( x+1, line );
			if ( pred == "edge" ) {
				++e;
				Edge *edge = getEdge( atoi( p[ 0 ].c_str() ) );
				Node *m = getNode( atoi( p[ 1 ].c_str() ) );
				Node *n = getNode( atoi( p[ 2 ].c_str() ) );
				edge->insertSegment( m->id, n->id );
				m->edges.push_back( edge );
			}
			else if ( pred == "hasCoordinates" ) {
				Node *n = getNode( atoi( p[ 0 ].c_str() ) );
				n->x = strtod( p[ 4 ].c_str(), 0 );
				n->y = strtod( p[ 3 ].c_str(), 0 );
			}
			else if ( pred == "isA" ) {
				types.insert( p[ 1 ] );
				setType( atoi( p[ 0 ].c_str() ), p[ 1 ] );
			}
			else if ( pred == "junction" )
				getNode( atoi( p[ 0 ].c_str() ) )->junction = true;
		}
		file.close();
	}
};
