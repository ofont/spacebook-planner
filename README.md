spacebook planner ICE interface
=================================

## Build

To build this, run `build.sh`.


## Planner dependencies

The list of dependencies should be as follows:

    apt-get install flex bison xutils-dev build-essential libjudy-dev scons

## ICE interface dependencies
    apt-get install python2.7 python-zeroc-ice

